<?php
// HTTP
define('HTTP_SERVER', 'http://buygreatclothes.com/');

// HTTPS
define('HTTPS_SERVER', 'http://buygreatclothes.com/');

// DIR
define('DIR_APPLICATION', 'C:/MAMP/htdocs/buygreatclothes.com/trunk/catalog/');
define('DIR_SYSTEM', 'C:/MAMP/htdocs/buygreatclothes.com/trunk/system/');
define('DIR_LANGUAGE', 'C:/MAMP/htdocs/buygreatclothes.com/trunk/catalog/language/');
define('DIR_TEMPLATE', 'C:/MAMP/htdocs/buygreatclothes.com/trunk/catalog/view/theme/');
define('DIR_CONFIG', 'C:/MAMP/htdocs/buygreatclothes.com/trunk/system/config/');
define('DIR_IMAGE', 'C:/MAMP/htdocs/buygreatclothes.com/trunk/image/');
define('DIR_CACHE', 'C:/MAMP/htdocs/buygreatclothes.com/trunk/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/MAMP/htdocs/buygreatclothes.com/trunk/system/storage/download/');
define('DIR_LOGS', 'C:/MAMP/htdocs/buygreatclothes.com/trunk/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/MAMP/htdocs/buygreatclothes.com/trunk/system/storage/modification/');
define('DIR_UPLOAD', 'C:/MAMP/htdocs/buygreatclothes.com/trunk/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'bgc_dev');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
