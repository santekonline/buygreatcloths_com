<div id="banner<?php echo $module; ?>" class="banner bt-banner">
 <div class="row home-masonry fadeInUp animated" data-animate="fadeInUp" data-delay="300">
	<div class="col-md-8">
	<a href="">
		<div class="col-md-12 left item">
			<div class="hovereffect">
				<img src="<?php echo $banners[0]['image']; ?>">
				 <div class="overlay">
		                <h2><?php echo $banners[0]['title'] ?></h2>
            		</div>
			</div>
		</div>
	</a>

		<div class="col-md-6 left item" id="skirts">
			<div class="hovereffect">
				<img src="<?php echo $banners[1]['image'] ?>">
				 <div class="overlay">
		                <h2><?php echo $banners[1]['title'] ?></h2>
            		</div>
            	</div>
		</div>

		<div class="col-md-6 left item">
			<div class="hovereffect">
				<img src="<?php echo $banners[2]['image'] ?>">
				 <div class="overlay">
		                <h2><?php echo $banners[2]['title'] ?></h2>
            		</div>
			</div>
		</div>

		<div class="col-md-6 left item">
			<div class="hovereffect">
				<img src="<?php echo $banners[3]['image']; ?>">
				 <div class="overlay">
		                <h2><?php echo $banners[3]['title'] ?></h2>
            		</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="col-md-12 right item">
			<div class="hovereffect">
				<img src="<?php echo $banners[4]['image']; ?>">
				 <div class="overlay">
		                <h2><?php echo $banners[4]['title'] ?></h2>
            		</div>
			</div>
		</div>
		<div class="col-md-12 right item">
			<div class="hovereffect">
				<img src="<?php echo $banners[5]['image']; ?>">
				 <div class="overlay">
		                <h2><?php echo $banners[5]['title'] ?></h2>
            		</div>
			</div>
		</div>
	</div>
</div>